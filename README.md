# Parent Repo for Screening Projects

## Pricing Project (Section A : Making a simple website with vanilla JS)

> [![Live Link](https://img.shields.io/badge/REPO-LINK-green)](https://gitlab.com/yashpundhir1/pricing-project)

---

## Lazy Loading Project (Section B: Lazy Loading To Avoid Pagination (vanilla JS))

> [![Live Link](https://img.shields.io/badge/REPO-LINK-green)](https://gitlab.com/yashpundhir1/lazy-loading-project)

---

## Project Description Video Link

> [Video Link](https://drive.google.com/file/d/1o-2EYi2JKeO_v2VBa0gAZXQrEAzECznT/view?usp=sharing)